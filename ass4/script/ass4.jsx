"use strict";
const apiKey = "cb09accfd3530ff38c93e85a5934ff29";

class Weather extends React.Component{
    constructor(props){
        super(props);
        let initial = {
            name: "",
            temp: "",
            iconurl: "",
        }
        this.state = {jsonstatus: initial}
    }


    componentDidMount(){
        this.fetchWeather(this.props.city);
    }


    fetchWeather(city){
        fetch("https://api.openweathermap.org/data/2.5/weather?q="+city+"&appid="+apiKey+"")
        .then(response =>{
            if(response.ok){
                return response.json();
            }
            else{
                let errorState = {
                    name: "Weather is not available at this time",
                    temp: "",
                    iconurl: "https://emojiisland.com/products/sad-iphone-emoji-jpg",
                }
                this.setState({jsonstatus: errorState});
                throw new Error("Status code: " + response.statusCode);
            }
        })
        .then(fetchedweatherinfo =>{
            let goodResponse = {
                name: fetchedweatherinfo.weather[0].description,
                temp: (parseInt(fetchedweatherinfo.main.temp-273.15, 10) + "°C"),
                iconurl: "http://openweathermap.org/img/wn/"+fetchedweatherinfo.weather[0].icon+"@2x.png",
            }
            this.setState({jsonstatus: goodResponse});
        })
        .catch(error => {
            console.error("Error code " + error);
        })
    }


    render(){
        let descripton = this.state.jsonstatus.name + " " + this.state.jsonstatus.temp;
        let weathericon = this.state.jsonstatus.iconurl;
        return ( 
            <div>
                <h3>
                    React Weather Component
                </h3>
                {descripton}
                <br></br>
                <img src={weathericon} alt="icon"></img>
                <button type="button" onClick={ () => this.fetchWeather(this.props.city)}>
                    Refresh
                </button>
            </div>
        );
    }
}


const domContainer = document.querySelector('#container');
ReactDOM.render(<Weather city="montreal"/>, domContainer);